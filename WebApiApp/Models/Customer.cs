﻿using System;
namespace WebApiApp.Models
{
    public class Customer
    {
        public int Id { get; set; }

        public string PhoneNumber { get; set; }
    }
}
