﻿using System.ComponentModel.DataAnnotations;
using LS.Helpers.Hosting.API;
using WebApiApp.SQRS;

namespace WebApiApp.Commands.Registration
{
    public class RegistrationCommand : ICommand<ExecutionResult>
    {
        [Required]
        public string PhoneNumber { get; set; }
    }
}