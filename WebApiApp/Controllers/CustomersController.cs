﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LS.Helpers.Hosting.API;
using LS.Helpers.Hosting.Extensions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using WebApiApp.Commands.Registration;

namespace WebApiApp.Controllers
{
    [ApiController]
    [Route("api/customers")]
    [ApiExplorerSettings(GroupName = "v1")]
    public class CustomersController : Controller
    {
        private readonly IMediator _mediator;

        public CustomersController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        // GET: api/values
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET api/values/5
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/values
        [HttpPost("registration")]
        [SwaggerOperation("Registration new devices")]
        [Produces("application/json", "application/xml")]
        [ProducesResponseType(typeof(ExecutionResult), 200)]
        public async Task<IActionResult> Registration([FromBody] RegistrationCommand registrationCommand)
        {
            var result = await _mediator.Send(registrationCommand);

            return this.FromExecutionResult(result);
        }

        // PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        // DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
