﻿using MediatR;

namespace WebApiApp.SQRS
{
    public interface IQuery<out T> : IRequest<T>
    {
    }
}