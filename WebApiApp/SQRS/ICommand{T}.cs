﻿using MediatR;

namespace WebApiApp.SQRS
{
    public interface ICommand<out T> : IRequest<T>
    {
    }
}